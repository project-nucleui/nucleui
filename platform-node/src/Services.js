import vm from 'vm';
import fs from 'fs';
import path from 'path';

const Services = new WeakMap();

const getMethodNameByRequest = (svcInst, reqPath, reqMethod) => {
	const methodName = Object.getOwnPropertyNames(
		svcInst.constructor.prototype
	).filter(
		v => v.indexOf('/') > -1
	).find((v) => {
		const parts = v.split(':');
		const reg = new RegExp(parts.pop().replace(/(\$[^/]+)/g, '(.+?)'));
		const res = reg.test(reqPath);

		if (parts.length > 0) {
			return parts[0].toUpperCase() === reqMethod.toUpperCase() && res;
		}

		return res;
	});

	if (!methodName) return [void 0, void 0];

	if (methodName.indexOf('$')) {
		const regArgs = [];
		const reg = new RegExp(methodName.split(':').pop().replace(/(\$[^/]+)/g, (_, match) => {
			regArgs.push(match.substr(1));
			return '([^/]+)';
		}));

		return [methodName, (reqPath.match(reg) || []).slice(1).reduce((v, cV, cI) => (
			{ ...v, [regArgs[cI]]: cV }
		), {})];
	}

	return [methodName, {}];
};

export default (NeueUI) => {
	Object.defineProperty(NeueUI, 'Services', {
		value: new (class ServerServices {
			constructor() {
				Services.set(this, {});

				NeueUI.Logger.attach(this, 'NodeServices');
				this.log('Initializing');

				this.init();
			}

			get(endpoint, body, headers) {
				return this.call({ method: 'GET', endpoint, body }, headers);
			}
			post(endpoint, body, headers) {
				return this.call({ method: 'POST', endpoint, body }, headers);
			}
			put(endpoint, body, headers) {
				return this.call({ method: 'PUT', endpoint, body }, headers);
			}
			delete(endpoint, body, headers) {
				return this.call({ method: 'DELETE', endpoint, body }, headers);
			}

			init() {
				NeueUI.Dispatcher.when('service').then((pkg) => {
					const { req, res, appContext } = pkg;
					const reqPath = pkg.path;

					this.log(['[REST]', `${req.method.toUpperCase()} /${reqPath}`]);

					this.call({
						req,
						res,
						appContext,
						path: reqPath,
						endpoint: reqPath,
						method: req.method,
						body: req.body
					}).then((response) => {
						try {
							res.status(200).json(response);
							this.log('Response Sent');
							this.log(response, this.LOG_DEBUG);
						} catch (error) {
							this.log(error);
							res.status(500).json({ status: 500, error });
						}
					}).catch((error) => {
						if (error.message && error.message.indexOf('404') > -1) {
							res.status(404);
							return;
						}

						this.log(error);
						res.status(500).json({ status: 500, error });
					});
				}).catch(err => this.log(err));
			}

			call({ method, endpoint, body }, headerArgs = {}) {
				const req = {
					path: endpoint,
					method: method || 'POST',
					body: body || '',
					headers: { ...headerArgs }
				};

				return new Promise((resolve, reject) => {
					const [, svc, ...endpointArr] = req.path.substr(1).split('/');
					req.endpoint = `/${endpointArr.join('/')}`;

					const Cache = Services.get(this);
					if (!(svc in Cache)) this.importServiceFile(svc);

					NeueUI.Dispatcher.when(`service.${svc}.loaded`).once((svcInst) => {
						const [
							methodName,
							params
						] = getMethodNameByRequest(svcInst, req.endpoint, req.method);

						if (methodName === void 0) return reject('404');

						const ret = svcInst[methodName].call(svcInst, {
							params, ...req
						}, (error, res) => {
							if (error !== void 0) {
								reject(error);
							} else {
								resolve(res);
							}
						});

						if (ret === void 0) return void 0;

						try {
							resolve(ret);
						} catch (error) {
							reject(error);
						}

						return void 0;
					}).catch((error) => {
						this.log(['ERROR', 'Error calling service', error]);
						this.log(['ERROR', 'Error details:', req], this.LOG_DEBUG);

						reject(error);

						throw error;
					});
				});
			}

			importServiceFile(svcName) {
				this.log(`Importing Service ${svcName}`, this.LOG_LOUD);

				/**
				 * @TODO Improve this!! There's gotta be a better way to find
				 * the source file for the service we're looking for...
				 */
				const refPath = path.resolve(path.join(
					NeueUI.Config.appContext, '..', '.tmp', 'src', 'services', `${svcName}.js`
				));

				this.log([svcName, `Opening compiled services file: '${refPath}'`], this.LOG_DEBUG);

				fs.readFile(refPath, 'utf8', (err, src) => {
					if (err) {
						NeueUI.Dispatcher.reject(`service.${svcName}.loaded`, err);
						this.log(err);
						return;
					}

					this.log([svcName, 'Service file opened successfully'], this.LOG_DEBUG);

					const opts = {
						filename: refPath,
						displayErrors: true
					};

					const scrt = new vm.Script(src, opts);

					this.log([svcName, 'Executed script in new context'], this.LOG_DEBUG);

					const cntx = vm.createContext(Object.assign({
						NeueUI,
						module: { exports: {} },
						exports: {}
					}, global));

					const scriptInit = scrt.runInNewContext(cntx, opts);

					const Cache = Services.get(this);
					Cache[svcName] = NeueUI.use(scriptInit);

					NeueUI.Dispatcher.resolve(`service.${svcName}.loaded`, Cache[svcName]);
				});
			}
		})(),
		enumerable: false,
		configurable: false,
		writable: false
	});

	return NeueUI.Services;
};

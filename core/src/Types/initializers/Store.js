const normalizeTypeName = typeName => typeName.substr(0, 1).toUpperCase() + typeName.substr(1).toLowerCase();

import { ResourceRef, ValueTypeFilters } from 'nui-utils';
import StoreObjectWrapper from '../classes/StoreObjectWrapper';

export default NeueUI => class StoreInitializer extends NeueUI.Initializers.Base {
	constructor(ResourcePkg) {
		super(ResourcePkg, false);

		const comp = ResourcePkg.component;

		let storeInst;
		let storeInterface;

		if (ValueTypeFilters.HasPrototype(comp)) {
			this.init();

			storeInst = new comp();
			storeInterface = storeInst.getInterface();
		} else if (ValueTypeFilters.Object(comp)) {
			this._pkg.component = StoreObjectWrapper(NeueUI);
			const Factory = this._pkg.component;
			this.init();
			storeInst = new Factory(comp);
			storeInterface = storeInst.getInterface();
		}

		this.Component = storeInterface;

		this._inst = storeInst;

		if (ValueTypeFilters.Function(ResourcePkg.component.prototype.initialState)) {
			storeInst.set(storeInst.initialState()).then(this.checkReady.bind(this));
		} else this.checkReady();
	}

	checkReady() {
		if (ValueTypeFilters.Function(this._inst.init)) this._inst.init();

		if (!this.testReady()) {
			this._inst.getInterface().subscribe(this.notifyReady.bind(this));
			return;
		}
		this.notifyReady();
	}

	notifyReady() {
		if (!this.testReady()) return;
		NeueUI.Dispatcher.resolve(`nui.resource.ready.${String(this.ref).replace(/[$:]/g, '.')}`, this.getInterface());
	}

	testReady() {
		return (
			!ValueTypeFilters.Function(this._pkg.component.prototype.isReady) ||
			this._inst.isReady() === true
		);
	}
};

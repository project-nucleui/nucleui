import { ResourceRef, ValueTypeFilters } from 'nui-utils';
import ActionObjectWrapper from '../classes/ActionObjectWrapper.js';

const normalizeTypeName = typeName => typeName.substr(0, 1).toUpperCase() + typeName.substr(1).toLowerCase();

export default NeueUI => class ActionInitializer extends NeueUI.Initializers.Base {
	constructor(ResourcePkg) {
		super(ResourcePkg, false);

		const { component } = ResourcePkg;

		if (ValueTypeFilters.HasPrototype(component)) {
			this.init();
			this.Component = new component();
			this.autoBindMethods();
		} else if (ValueTypeFilters.Object(component)) {
			this._pkg.component = ActionObjectWrapper(NeueUI);
			const Factory = this._pkg.component;
			this.init();
			this.Component = new Factory(component);
		}

		NeueUI.Dispatcher.resolve(`nui.resource.ready.${String(this.ref).replace(/[$:]/g, '.')}`, this.getInterface());
	}

	autoBindMethods() {
		const { prototype } = this._pkg.component;
		const methods = Object.getOwnPropertyNames(prototype);

		methods.forEach((v) => {
			if (v === 'constructor' || !ValueTypeFilters.Function(prototype[v])) return;
			this.Component[v] = prototype[v].bind(this.Component);
		});
	}
};

import React from 'react';
import ReactDOM from 'react-dom';
import { ValueTypeFilters } from 'nui-utils';

export default NeueUI => class Component extends React.Component {
	constructor(props) {
		super(props);

		NeueUI.Logger.attach(this, `component/${this.ref}`);

		this._class = this.constructor;
		this.state = this.state || {};
		this._mounted = false;

		this.log(`Instantiating. Dependencies are${this._class.nui_depsReady ? ' ' : ' NOT '}ready.`, this.LOG_DEBUG);

		if (this._class.nui_depsReady) {
			this.nui_Ready();
		} else {
			NeueUI.Dispatcher.when(`nui.resource.ready.${String(this.ref).replace(/[$:]/g, '.')}`).then(() => this.nui_Ready());
		}
	}

	nui_SetState(newState) {
		if (this._mounted === true) {
			this.setState(newState);
		} else {
			Object.assign(this.state, newState);
		}
	}

	nui_Ready() {
		this.log('Component ready', this.LOG_LOUD);

		this.nui_SetState({ nui_Ready: true });

		this.nui_initDeps();
	}

	nui_initDeps() {
		this.log('Initializing dependencies', this.LOG_LOUD);
		const Stores = this._class._stores || [];
		const storeRefs = Object.keys(Stores);
		const initialState = {};

		if (storeRefs.length > 0) {
			for (const storeRef of storeRefs) {
				const store = Stores[storeRef].Component;

				this.nui_initStoreDep(store);

				this.log(`Grabbing state from store: ${store.ref}`, this.LOG_DEBUG);

				initialState[storeRef] = store.get();
			}
		}

		this.nui_SetState(initialState);
	}

	nui_initStoreDep(store) {
		this.log('Attaching to store', this.LOG_DEBUG);

		store.subscribe(() => {
			this.nui_setStateFromStores(store);
		});
	}

	nui_setStateFromStores(store) {
		this.log([store.ref, 'Store changed; updating state based on subscription to'], this.LOG_DEBUG);

		const newState = {};
		newState[store.ref] = store.get();
		this.nui_SetState(newState);
	}

	nui_preRender() {
		this._mounted = true;

		this.log('Pre-Render Executing', this.LOG_DEBUG);

		if (this.state.nui_Ready === true && this._class.nui_depsReady === true) {
			this.log('Ready; Rendering component...', this.LOG_DEBUG);

			setTimeout(() => NeueUI.Dispatcher.resolve(`nui.component.render.${String(this.ref).replace(/[$:]/g, '.')}`, true), 0);

			return this.devRender.apply(this, arguments);
		} else if (ValueTypeFilters.Function(this.renderWaiting)) {
			this.log('Not Ready; Executing renderWaiting...', this.LOG_DEBUG);

			return this.renderWaiting.apply(this, arguments);
		}
		this.log('Not Ready; Waiting...', this.LOG_DEBUG);

		return <div />;
	}
};

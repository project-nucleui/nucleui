import { ResourceRef } from 'nui-utils';
import { MutableThenable } from 'thenable-events';

const defineImmutableProp = (name, val, obj) => {
	Object.defineProperty(obj, name, {
		configurable: false,
		writable: false,
		enumerable: false,
		value: val
	});
};

const PromiseMap = new WeakMap();
const Registry = new WeakMap();

function requireRef(ref) {
	if (!ref) throw new TypeError(`Unable to get resource: Bad resource ref: \`${ref}\``);

	let resRef;
	try {
		resRef = new ResourceRef(ref);
	} catch (e) {
		this.log(e);
		throw new TypeError(`Unable to get resource: Bad resource ref: \`${resRef}\``);
	}

	return resRef;
}

export default NeueUI => new (class ResourceManager {
	constructor() {
		NeueUI.Logger.attach(this, 'Resources');

		PromiseMap.set(this, {});
		Registry.set(this, {});

		defineImmutableProp('get', this.get.bind(this), NeueUI);
		defineImmutableProp('import', this.import.bind(this), NeueUI);
		defineImmutableProp('register', this.register.bind(this), NeueUI);
	}

	get(ref) {
		const resRef = requireRef.call(this, ref);

		const cache = PromiseMap.get(this);
		if (cache[resRef]) return cache[resRef];

		cache[resRef] = new MutableThenable((res, rej) => {
			let resolved = false;

			NeueUI.Dispatcher.when(`nui.resource.ready.${String(resRef).replace(/[$:]/g, '.')}`).once(
				(pkg) => { resolved = true; res(pkg); },
				(e) => { resolved = true; rej(e); }
			);

			setTimeout(() => {
				if (resolved === true) return;
				NeueUI.Dispatcher.resolve('nui.resource.import', resRef);
			}, 0);
		}, e => this.log(e));

		return cache[resRef];
	}

	import(...args) { return this.get(...args); }

	register(ref, incl) {
		const resRef = requireRef.call(this, ref);

		if (Registry.get(this)[resRef]) return;

		const pkg = { ref: resRef };

		Registry.get(this)[resRef] = pkg;

		this.log(`Registering resource: ${resRef.toString()}`, this.LOG_DEBUG);

		pkg.dependencies = incl.dependencies;

		if (pkg.ref.type === 'actions') {
			pkg.component = incl.factory(NeueUI.Interfaces.Action);
			(new NeueUI.Initializers.Action(pkg));
		} else if (pkg.ref.type === 'stores') {
			pkg.component = incl.factory(NeueUI.Interfaces.Store);
			(new NeueUI.Initializers.Store(pkg));
		} else if (typeof incl.factory === 'function') {
			pkg.component = incl.factory(NeueUI.Interfaces.Component);
			(new NeueUI.Initializers.Component(pkg));
		} else {
			pkg.component = incl.component;
			(new NeueUI.Initializers.Component(pkg));
		}
	}

	log() { }
})();

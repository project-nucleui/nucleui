import { ValueTypeFilters } from 'nui-utils';

const Stores = new WeakMap();

const deepAssign = (host, ...objs) => {
	if (objs.length < 1) return host;
	let nxtObj;
	if (objs.length > 1) {
		nxtObj = deepAssign.apply(this, objs);
	}

	for (const k of nxtObj) {
		host[k] = getValueForDeepAssign(nxtObj, k);
	}

	return host;
};

function getValueForDeepAssign(v, i) {
	if (ValueTypeFilters.ObjectLiteral(v)) return deepAssign({}, v);
	if (ValueTypeFilters.Array(v)) return [].concat(v).map(getValueForDeepAssign);
	if (ValueTypeFilters.Primitive(v)) return v;
	throw new TypeError('Attempted to set a store value to a non-primitive');
}

export default NeueUI => class Store {
	constructor() {
		Stores.set(this, {});
	}

	get() {
		const ret = {};

		Object.assign(ret, Stores.get(this));
	}
};

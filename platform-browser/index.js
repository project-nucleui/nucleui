module.exports = {
	Loader: require('./lib/Loader.js').default,
	Renderer: require('./lib/Renderer.js').default,
	Services: require('./lib/Services.js').default,
	History: require('./lib/History.js').default
};

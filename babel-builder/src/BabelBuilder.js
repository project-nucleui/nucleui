import fs from 'fs-extra';
import path from 'path';
import * as babel from 'babel-core';

export default class BabelBuilder {
	constructor(src, dest, Compiler) {
		Compiler.Logger.attach(this, 'BabelBuilder');

		this.log('Initializing');

		this.src = src;
		this.dest = dest;

		this.compiler = Compiler;
		this.getFolders = Compiler.getFolders.bind(Compiler);
		this.buildContext = Compiler.Config.BuildContext;

		this.build(src, dest);

		this.log('Complete');
		Compiler.dispatcher.resolve('BabelBuilder', true);
	}

	build(src, dest) {
		this.log('Transpiling', this.LOG_LOUD);

		const Compiler = this.compiler;

		const folders = this.getFolders(src).filter(
			dir => dir.indexOf('node_modules') < 0
		).map((dir) => {
			this.build(
				path.resolve(path.join(src, dir)),
				path.resolve(path.join(dest, dir))
			);
		});

		const dirsMade = fs.mkdirpSync(dest);

		return this.processFiles(src, dest);
	}

	processFiles(src, dest) {
		this.log('Processing files', this.LOG_LOUD);
		return this.getFiles(src).map(file => this.writeFile(file, src, dest));
	}

	writeFile(file, src, dest) {
		const Compiler = this.compiler;

		const { code, err } = this.transformFile(file, src);

		if (err) Compiler.compileError(err);

		this.log(`Writing file: ${dest + file}`, this.LOG_DEBUG);

		return fs.writeFileSync(
			this.renameJsx(
				path.resolve(
					path.join(dest, file)
				)
			), code
		);
	}

	transformFile(file, src) {
		this.log(`Transforming file: ${src}${file}`, this.LOG_DEBUG);

		const projectRoot = path.resolve(this.src);
		const moduleRoot = path.join(
			path.resolve(this.buildContext), './node_modules'
		);

		const ret = { code: null, err: null };

		const babelrc = this.importJson(
			path.join(this.buildContext, '.babelrc')
		) || this.importJson(
				path.join(
					path.resolve(__dirname, '..'), '.babelrc'
				)
			) || {};

		try {
			ret.code = babel.transformFileSync(
				path.resolve(path.join(src, file)),
				Object.assign({
					filename: file,
					moduleRoot,
					resolveModuleSource: (source, filename) => {
						if (
							source.indexOf('/') > -1 ||
							source.indexOf('\\') > -1
						) {
							const resolved = path.resolve(
								path.dirname(filename), source
							);

							if (resolved.indexOf(projectRoot) > -1) return this.renameJsx(source);
							return source;
						}

						return source;
					}
				}, babelrc)
			).code;
		} catch (e) {
			ret.err = e;
		} finally {
			return ret;
		}
	}

	renameJsx(filename) {
		const parts = filename.split(/\./g);
		if (parts[parts.length - 1] === 'jsx') {
			parts.pop();
			return parts.concat('js').join('.');
		} return filename;
	}

	getFiles(dir) {
		this.log(`Retrieving file listing for \`${dir}\``, this.LOG_DEBUG);
		return fs.readdirSync(dir).filter(file =>
			fs.statSync(path.join(dir, file)).isFile() &&
			(
				file.split(/\./g).pop() === 'jsx' ||
				file.split(/\./g).pop() === 'js'
			)
		);
	}

	getFolders(dir) {
		this.log(`Retrieving directory listing for \`${dir}\``, this.LOG_DEBUG);
		return fs.readdirSync(dir).filter(file => fs.statSync(path.join(dir, file)).isDirectory());
	}

	importJson(filename) {
		try {
			const contents = fs.readFileSync(
				path.resolve(filename),
				{ encoding: 'utf-8' }
			);

			return JSON.parse(contents);
		} catch (e) {
			return false;
		}
	}
}

import fs from 'fs';
import path from 'path';

export default (BUILD_ARGS) => {
	const Config = {};
	const FileConfig = { Build: {}, Server: {} };

	const CWD = path.resolve(process.cwd());

	if (BUILD_ARGS.C) {
		const configPath = typeof BUILD_ARGS.C === 'boolean' ? '.nuiconfig' : BUILD_ARGS.C;
		const ConfigSearchPath = path.join(CWD, configPath);

		try {
			fs.accessSync(ConfigSearchPath);
			const cfg = require(ConfigSearchPath);
			if (typeof cfg !== 'undefined') {
				if (typeof cfg.Build !== 'undefined') Object.assign(FileConfig.Build, cfg.Build);
				if (typeof cfg.Server !== 'undefined') Object.assign(FileConfig.Server, cfg.Server);
			}
		} catch (e) {
			// intentionally blank: We don't care if they don't have a config file
		}
	}

	Config.Build = Object.assign({
		get Target() { return BUILD_ARGS.t; },
		get BuildContext() { return CWD; },
		get NuiOutputFolder() { return path.join(Config.Build.BuildContext, './.nui/'); },
		get TempOutputFolder() { return path.join(Config.Build.NuiOutputFolder, './tmp/'); },
		get ResourceMapsFolder() { return path.join(Config.Build.NuiOutputFolder, './maps/'); },
		get MetaOutputFolder() { return path.join(Config.Build.NuiOutputFolder, './meta/'); },
		get BuildPublicOutputFolder() { return path.join(Config.Build.NuiOutputFolder, './dist/pub'); },
		get BuildServerOutputFolder() { return path.join(Config.Build.NuiOutputFolder, './dist/svr'); },
		get SourcePath() { return path.join(Config.Build.BuildContext, './src/'); },
		get DefaultModulePath() { return path.join(Config.Build.NuiOutputFolder, './src/'); },
		get NodeSiteModulesPath() { return path.join(Config.Build.SourcePath, './.modules/'); },
		get ClientScriptPath() { return path.join(__dirname, './client.js'); },
		get ServerScriptPath() { return path.join(__dirname, './server.js'); },
		get CleanPaths() {
			return [
				Config.Build.NodeSiteModulesPath,
				Config.Build.NuiOutputFolder
			];
		}
	}, FileConfig.Build);

	Config.Server = Object.assign({
		port: 3030,
		host: void 0,
		get public() { return path.join(Config.Build.BuildContext, 'public'); }
	}, FileConfig.Server);

	return Config;
};

import fs from 'fs';
import path from 'path';
import express from 'express';
import bodyParser from 'body-parser';
import Logger from 'nui-logger';

const AppLogger = new Logger({ namespace: 'NUI-Simple' });

export default (Config) => {
	const RouteHandler = require(
		path.join(Config.Build.BuildServerOutputFolder, './app.js')
	).default;

	const app = express();

	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));

	app.all(/.+/, (req, res) => {
		const reqPath = req.path;

		if (reqPath !== '/' && reqPath.indexOf('.') !== -1) {
			try {
				const searchPath = path.join(
					Config.Build.BuildPublicOutputFolder, reqPath
				);

				fs.accessSync(searchPath);
				return res.sendFile(searchPath);
			} catch (e) {
				try {
					const searchPath = path.join(Config.Server.public, reqPath);

					fs.accessSync(searchPath);
					return res.sendFile(searchPath);
				} catch (e2) {
					// @TODO: Throw a 404
					return res.status(404).send('404');
				}
			}
		}

		return RouteHandler(req, res, Config);
	});

	app.listen(Config.Server.port || 3030, Config.Server.host);


	let port = ':3030';
	if (Config.Server.port) {
		port = (Config.Server.port === 80) ? '' : `:${Config.Server.port}`;
	}

	AppLogger.log(`App started at http://${Config.Server.host || '0.0.0.0'}${port}/`);
};

# Quick start

**GUIDES AND REFERENCES COMING SOON TO: http://neuelogic.com/**

Want to know how to create a `nui` project from scratch?

```sh
$ npm i -g yarn
$ git init .
$ yarn init
$ yarn add nui-simple
$ mkdir -p src/pages/ src/actions src/views src/stores
$ touch src/actions/actions.js
$ touch src/pages/home.jsx
$ touch src/stores/store.js
$ touch src/views/helloWorld.jsx
$ git commit -a -m "Initial commit"
$ atom .
```

Then add some content to your **`src/pages/home.jsx`** file:

```javascript
import React from 'react';

export default (nui) => () => (
	<b>Hello World!</b>
);
```

Add your start script to your **`package.json`** file:
```json
...
	"scripts": {
		"start": "nui -b"
	}
...
```

And start it up!
```sh
$ yarn start
```

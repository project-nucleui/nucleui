#!/bin/sh

for ELEMENT in utils logger babel-builder build core platform-browser platform-node simple
do
	cd $ELEMENT

	[ $1 -eq 'clean' ] && rm -rf node_modules lib

	yarn
	yarn run build
	cd ..
done

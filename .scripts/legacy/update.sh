#!/bin/bash

cd logger/ && echo -n "nui-logger: " && git pull
cd ../utils/ && echo -n "nui-utils: " && git pull
cd ../babel-builder/ && echo -n "nui-builder-babel: " && git pull
cd ../build/ && echo -n "nui-build: " && git pull
cd ../core/ && echo -n "nui-core: " && git pull
cd ../platform-browser/ && echo -n "nui-platform-browser: " && git pull
cd ../platform-node/ && echo -n "nui-platform-node: " && git pull
cd ../test-module/ && echo -n "npm-pkg-test: " && git pull
cd ../test-site/ && echo -n "test-site: " && git pull && cd ..

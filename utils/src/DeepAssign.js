import ValueTypeFilters from './ValueTypeFilters.js';

function getValueForDeepAssign(v) {
	if (ValueTypeFilters.Primitive(v)) return v;
	if (ValueTypeFilters.Array(v)) return [].concat(v).map(getValueForDeepAssign);
	if (ValueTypeFilters.ObjectLiteral(v)) return DeepAssign({}, v);
	throw new TypeError(`Safe DeepAssign only allows primitives, arrays, and object-literals. Attempted to use type: ${Object.prototype.toString(v)}`);
}

export default function DeepAssign(host, ...objs) {
	if (objs.length < 1) return DeepAssign({}, host);
	let nxtObj;
	if (objs.length > 1) {
		nxtObj = DeepAssign(...objs);
	} else {
		nxtObj = objs[0];
	}

	for (const k in nxtObj) {
		host[k] = getValueForDeepAssign(nxtObj[k]);
	}

	return host;
}

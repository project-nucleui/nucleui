let ValueTypeFilters;

export default ValueTypeFilters = {
	All: val => !!(val),
	Boolean: val => !!val === val,
	Primitive: val => (
		ValueTypeFilters.String(val) ||
		ValueTypeFilters.Number(val) ||
		ValueTypeFilters.Boolean(val)
	),
	HasPrototype: val => ValueTypeFilters.Function(val) && val.hasOwnProperty('prototype') && ValueTypeFilters.Object(val.prototype),
	ClassInstance: (val) => { try { return val && val.constructor && ValueTypeFilters.Function(val.constructor); } catch (e) { return false; } },
	Function: val => val && typeof val === 'function',
	String: val => typeof val === 'string',
	Object: val => val && typeof val === 'object',
	ObjectLiteral: val => val && Object.prototype.toString.apply(val) === '[object Object]',
	Array: val => val && Object.prototype.toString.call(val) === '[object Array]',
	Number: val => typeof val === 'number',
	JSON: (val) => { try { JSON.parse(val); } catch (e) { return false; } return true; },
	React: val => val.render && ValueTypeFilters.Function(val.render)
};

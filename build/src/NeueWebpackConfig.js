import path from 'path';

export var CommonConfig = {
	name: 'common',
	filename: 'common.js',
	children: false,
	chunks: 'EntryPoints',
	minChunks: 'CompositeEntries.common.length'
};

export default (CompilerConfig, CompositeEntries, Plugins) => ({
	// debug: (CompilerConfig.BuildArgs || {}).hasOwnProperty('verbose'),
	context: CompilerConfig.CompilerContextFolder || CompilerConfig.BuildContext,
	entry: CompositeEntries,
	output: {
		path: CompilerConfig.BuildOutputFolder,
		filename: 'compiled.[name].js'
	},
	// resolveLoader: {
	// 	modules: [path.join(CompilerConfig.BuildContext, 'node_modules')]
	// },
	resolve: {
		modules: [
			path.join(CompilerConfig.BuildContext, 'node_modules'),
			path.join(CompilerConfig.BuildContext)
		]
	},
	plugins: Plugins,
	module: {
		loaders: [
			{
				test: /.jsx?$/,
				loader: 'babel-loader',
				exclude: /node_modules/,
				query: {
					presets: ['env', 'react']
				}
			},
			{
				test: /.json$/,
				loader: 'json-loader'
			}
		]/* ,
		postLoaders: [
			{
				test: /\.js[x]?$/,
				exclude: /(node_modules)\/.+\/(node_modules)/,
				loader: CompilerConfig.ResourceLoaderPath
			}
		] */
	}
});

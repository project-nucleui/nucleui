import NeueResourceBuilder from './NeueResourceBuilder.js';

/*
	Create a CommonsChunkPlugin for exclusive peerDependencies
	Delegate entryPoint creation to NeueResourceBuilder
*/

export default class NeueModuleBuilder {
	constructor(compiler, analyzer) {
		compiler.Logger.attach(this, 'NeueModuleBuilder');

		this.compiler = compiler;
		this.dispatcher = compiler.dispatcher;
		this.analyzer = analyzer;

		this.buildWebpackObject();
	}

	buildWebpackObject() {
		this.log([this.analyzer.name, 'Building Resource Configurations'], this.LOG_LOUD);

		const Compiler = this.compiler;
		const Analyzer = this.analyzer;

		this.EntryPoints = {};
		/*		this.CompositeEntries = [];
		this.Commons = []; */

		this.dispatcher.when(`NeueResourceBuilder.${this.analyzer.name}`).then(
			this.handleResourceFinished.bind(this)
		).catch(this.compiler.compileError.bind(this.compiler));

		const Resources = Analyzer.files;

		this._res = Resources.map((v, i) => v.name);
		this._resDone = [];

		for (let i = 0; i < Resources.length; i++) {
			(new NeueResourceBuilder(Compiler, Resources[i]));
		}

		/*
			Grab base webpack configuration object
			Assemble NeueModuleBuilder and NeueResourceBuilder
				results into base webpack config
			Announce result once complete
		*/
	}

	handleResourceFinished(res) {
		this.log([res.resource.ref, 'Resource Configuration Built'], this.LOG_LOUD);

		this._resDone.push(res.name);

		this.EntryPoints[
			res.resource.ref.serialize()
		] = res.EntryPoints;

		this.processComplete();
	}

	processComplete() {
		const Compiler = this.compiler;

		if (!Compiler._compareArrs(
			this._res,
			this._resDone
		)) return;

		this.dispatcher.resolve(`NeueModuleBuilder.${this.analyzer.name}`, this);
	}
}

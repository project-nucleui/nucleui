import fs from 'fs-extra';
import { EOL } from 'os';
import path from 'path';
import { ResourceRef } from 'nui-utils';

const MAP_FILE_EXT = 'js';

let log = () => {};

export default (compiler) => {
	log = msg => compiler.log(['NeueRouteAggregator', msg]);

	log('Initializing');

	const { dispatcher } = compiler;
	const { resourceList } = compiler.BuildOutputObject.Meta;
	const Routes = {};

	try {
		log('Aggregating routes from page components');

		const routes = Object.keys(resourceList).filter(
			ref => (new ResourceRef(ref)).type === 'pages' && resourceList[ref].routes !== null
		).map(ref => Routes[ref] = resourceList[ref].routes);
	} catch (e) {
		return dispatcher.reject('NeueRouteAggregator', e), void 0;
	}

	dispatcher.resolve('NeueRouteAggregator', Routes);
};

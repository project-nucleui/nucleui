import fs from 'fs';
import util from 'util';
import { ResourceRef } from 'nui-utils';
import * as babylon from 'babylon';

const traverse = (o, func) => {
	let ret = [];
	for (const i in o) {
		if (!o[i]) continue;
		const r = func(o[i]);
		if (r) ret = [r].concat(ret);
		if (o[i] !== null && typeof o[i] === 'object') ret = ret.concat(traverse(o[i], func));
	}
	return ret;
};

export default class NeueResourceAnalyzer {
	constructor(compiler, res) {
		Object.assign(this, res);

		compiler.Logger.attach(this, 'NeueResourceAnalyzer');
		this.log(`Analyzing ${this.ref}`, this.LOG_LOUD);

		this.module = this.ref.module;

		this.compiler = compiler;
		this.dispatcher = compiler.dispatcher;

		this.analyze();
	}

	getDefaultExportDeclaration(bodyArr) {
		return bodyArr.reduce.call(bodyArr,
			(v, node) => (node.type === 'ExportDefaultDeclaration' ? node : v)
			, {}).declaration;
	}

	findNodesByType(obj, type) {
		return traverse(obj,
			v => ((v.type && v.type == type) ? v : void 0)
		).filter(v => v);
	}

	analyze() {
		this.dependencies = {};
		const AllDependencies = [];
		const DependenciesByType = { Actions: [], Stores: [], Views: [] };

		try {
			if (this.ref.type === 'views' || this.ref.type === 'pages') {
				const data = fs.readFileSync(this.path, 'utf8');

				const ast = babylon.parse(data, {
					sourceType: 'module',

					plugins: [
						'jsx',
						'flow',
						'doExpressions',
						'objectRestSpread',
						'classProperties',
						'exportExtensions',
						'asyncGenerators',
						'functionBind',
						'functionSent',
						'dynamicImport'
					]
				});

				const programBody = ast.program;

				const CallExpressions = this.findNodesByType(programBody, 'CallExpression');

				AllDependencies.push(...CallExpressions.filter(
					exp => (
						exp && exp.callee && exp.callee.object &&
						exp.callee.object.type === 'ThisExpression' &&
						(
							exp.callee.property.name === 'Views' ||
							exp.callee.property.name === 'Actions' ||
							exp.callee.property.name === 'Stores'
						)
					)
				).map(exp => (
					(
						new ResourceRef(
							exp.arguments[0].value
						)
					).build(
						exp.callee.property.name.toLowerCase(), this.module, true
					)
				)).filter(
					(exp, i, arr) => arr.findIndex(e => String(exp) === String(e)) === i
				));

				AllDependencies.forEach(dep => DependenciesByType[
					dep.type[0].toUpperCase() + dep.type.slice(1)
				].push(dep));

				if (this.ref.type === 'pages') {
					const NamedExports = this.findNodesByType(programBody, 'AssignmentExpression');

					this.routes = NamedExports.filter(
						exp => (
							exp.left.type === 'MemberExpression' &&
							exp.left.object.name === 'exports' &&
							exp.left.property.name === 'Routes' &&
							exp.right.name !== 'undefined'
						)
					).map((exp) => {
						const RoutesExp = exp.right;

						if (!RoutesExp) return false;

						const regexpPrefix = 'R$';
						switch (RoutesExp.type) {
							case 'RegExpLiteral':
								return `${regexpPrefix}${RoutesExp.extra.raw}`;
							case 'ArrayExpression':
								return [].filter.call(RoutesExp.elements, e =>
									e.type === 'StringLiteral' ||
									e.type === 'RegExpLiteral'
								).map(e =>
									((e.type === 'RegExpLiteral') ?
										`${regexpPrefix}${e.extra.raw}`
										: e.value)
								);
							case 'StringLiteral':
								return RoutesExp.value;
							default:
								throw new TypeError(`Invalid Type for Routes Export: ${RoutesExp.type}`);
						}
					}).reduce((v, cV) => (cV && v.concat(cV)) || v, []);
				}
			}
		} catch (e) {
			this.dispatcher.reject(`NeueResourceAnalyzer.${this.ref}`, e);
			return;
		} finally {
			Object.assign(
				this.dependencies,
				DependenciesByType,
				{ All: AllDependencies }
			);

			this.log(JSON.stringify(this.dependencies), this.LOG_DEBUG);
		}

		this.dispatcher.resolve(`NeueResourceAnalyzer.${this.ref}`, this);
	}
}

let Styler;

const styles = {
	bold: ['\x1B[1m', '\x1B[22m'],
	italic: ['\x1B[3m', '\x1B[23m'],
	underline: ['\x1B[4m', '\x1B[24m'],
	inverse: ['\x1B[7m', '\x1B[27m'],
	strikethrough: ['\x1B[9m', '\x1B[29m'],
	white: ['\x1B[37m', '\x1B[39m'],
	gray: ['\x1B[90m', '\x1B[39m'],
	grey: ['\x1B[90m', '\x1B[39m'],
	black: ['\x1B[30m', '\x1B[39m'],
	blue: ['\x1B[34m', '\x1B[39m'],
	cyan: ['\x1B[36m', '\x1B[39m'],
	green: ['\x1B[32m', '\x1B[39m'],
	magenta: ['\x1B[35m', '\x1B[39m'],
	red: ['\x1B[31m', '\x1B[39m'],
	yellow: ['\x1B[33m', '\x1B[39m']
};

if (
	!(typeof window !== 'undefined' && ({}).toString.call(window) === '[object Window]') &&
	(typeof global !== 'undefined' && ({}).toString.call(global) === '[object global]')
) {
	Styler = function (msgs) {
		let ret = '';
		let end = [].concat(msgs.pop()).join(' ');
		let error = false;

		for (const msg of msgs) {
			try {
				if ((`${msg}`).toLowerCase().split('\n').splice(0, 1)[0].indexOf('error') > -1) error = true;
			} catch (e) {}
			ret += `[${styles.cyan[0]}${msg}${styles.cyan[1]}] `;
		}

		if (end.toLowerCase().split('\n').splice(0, 1)[0].indexOf('error') > -1) error = true;

		end = end.replace(/\[([A-Za-z]+)\]/ig, `[${styles.yellow[0]}$1${styles.yellow[1]}]`);

		return [ret + (
			error ? `${styles.red[0]}${end}${styles.red[1]}` : end
		)];
	};
}

export default Styler;

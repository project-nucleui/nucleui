let Styler;

const styles = {
	bold: 'font-weight:bold;',
	italic: 'font-style:italic;',
	underline: 'text-decoration:underline;',
	inverse: 'background-color:black;color:white;',
	strikethrough: 'text-decoration:line-through;',
	white: 'color:white;',
	gray: 'color:gray;',
	grey: 'color:grey;',
	black: 'color:black;',
	blue: 'color:blue;',
	cyan: 'color:cyan;',
	green: 'color:green;',
	magenta: 'color:magenta;',
	red: 'color:red;',
	yellow: 'color:yellow;',
	reset: 'color:#444;'
};

if (
	(typeof window !== 'undefined' && ({}).toString.call(window) === '[object Window]') &&
	!(typeof global !== 'undefined' && ({}).toString.call(global) === '[object global]')
) {
	Styler = function (msgs) {
		const ret = [];
		const end = `%c${[].concat(msgs.pop()).join(' ')}`;
		const endStyles = [];
		let error = false;

		for (const msg of msgs) {
			try {
				if ((`${msg}`).toLowerCase().split('\n').splice(0, 1)[0].indexOf('error') > -1) error = true;
			} catch (e) {}

			ret.push('%c[');
			ret.push(`%c${msg}`);
			ret.push('%c] ');
			endStyles.push(styles.reset);
			endStyles.push(styles.magenta);
			endStyles.push(styles.reset);
		}

		if (end.toLowerCase().split('\n').splice(0, 1)[0].indexOf('error') > -1) error = true;

		endStyles.push(error ? styles.red : styles.reset);

		return [ret.concat(end).join('')].concat(endStyles);
	};
}

export default Styler;

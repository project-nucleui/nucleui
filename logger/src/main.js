import DefineLogLvlConsts from './consts.js';
import NodeStyler from './styler.node.js';
import BrowserStyler from './styler.browser.js';

export default class NeueLogger {
	constructor(config) {
		this.config = Object.assign({
			colors: true,
			namespace: void 0,
			showTimestamp: true,
			verbosity: this.LOG_QUIET
		}, config || {});

		this.styler = NodeStyler || BrowserStyler || function () { return arguments; };
	}

	log(msg, verbosity = this.LOG_QUIET) {
		if (this.config.verbosity < verbosity) return;

		console.log(...this.styler(
			[this.getTimestamp(), this.getNamespace()].concat(msg)
		));
	}

	attach(inst, namespace) {
		DefineLogLvlConsts(inst);

		Object.defineProperties(inst, {
			log: {
				enumerable: false,
				writable: false,
				configurable: false,
				value: (msg, verbosity) => this.log(
					(typeof namespace !== 'string') ? msg : [namespace, msg]
					, verbosity)
			}
		});
	}

	getNamespace() {
		if (typeof this.config.namespace !== 'string') return '';
		return this.config.namespace;
	}

	getTimestamp() {
		if (this.config.showTimestamp !== true) return '';

		const d = new Date();
		const hours = d.getHours();
		const mins = d.getMinutes();
		const secs = d.getSeconds();
		return `${
			hours < 10 ? `0${hours}` : hours
		}:${
			mins < 10 ? `0${mins}` : mins
		}:${
			secs < 10 ? `0${secs}` : secs
		}`;
	}
}

DefineLogLvlConsts(NeueLogger);
DefineLogLvlConsts(NeueLogger.prototype);
